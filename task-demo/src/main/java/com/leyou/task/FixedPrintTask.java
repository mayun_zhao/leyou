package com.leyou.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

/**
 * @author Administrator
 */
@Configuration
@EnableScheduling
public class FixedPrintTask {
    private Logger logger= LoggerFactory.getLogger(getClass());
    @Scheduled(cron = "*/15 * * *  * ?")
    private void configureTasks() {
        System.err.println("执行静态定时任务时间: " + LocalDateTime.now());
    }
}
