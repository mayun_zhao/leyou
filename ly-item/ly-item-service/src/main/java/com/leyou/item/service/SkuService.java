package com.leyou.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leyou.item.entity.Sku;

public interface SkuService extends IService<Sku> {
}
