package com.leyou.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.leyou.item.dto.SpecParamDTO;
import com.leyou.item.entity.SpecParam;
import com.leyou.item.mapper.SpecParamMapper;
import com.leyou.item.service.SpecParamService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpecParamServiceImpl extends ServiceImpl<SpecParamMapper, SpecParam> implements SpecParamService {
    @Override
    public List<SpecParamDTO> querySpecParams(Long groupId, Long categoryId, Boolean searching) {
        return SpecParamDTO.convertEntityList(
                query().eq(groupId != null, "group_id", groupId)
                        .eq(categoryId != null, "category_id", categoryId)
                        .eq(searching != null, "searching", searching)
                        .list()
        );
    }
}
