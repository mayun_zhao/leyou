package com.leyou.item.web;

import com.leyou.item.dto.CategoryDTO;
import com.leyou.item.entity.Category;
import com.leyou.item.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/of/parent")
    public ResponseEntity<List<CategoryDTO>> queryCategoryByParentId(@RequestParam("pid") Long pid) {
        return ResponseEntity.ok(CategoryDTO.convertEntityList(categoryService.query().eq("parent_id", pid).list()));
    }

    /**
     * 根据分类id 查询商品分类
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity<CategoryDTO>  queryCategoryById(@PathVariable("id") Long id){
        return  ResponseEntity.ok(new CategoryDTO(categoryService.getById(id)));
    }

    /**
     * 根据id集合查询分类集合
     * @param ids
     * @return
     */
    @GetMapping("list")
    public ResponseEntity<List<CategoryDTO>> queryCategoryByIds(@RequestParam("ids") List<Long> ids){
        return ResponseEntity.ok(CategoryDTO.convertEntityList(categoryService.listByIds(ids)));
    }

    /**
     * 根据品牌id查询分类集合
     * @param brandId
     * @return
     */

    @GetMapping("/of/brand")
    public ResponseEntity<List<CategoryDTO>> queryCategoryByBrandId(@RequestParam("id") Long brandId){
        return ResponseEntity.ok(categoryService.queryCategoryByBrandId(brandId));
    }
}
