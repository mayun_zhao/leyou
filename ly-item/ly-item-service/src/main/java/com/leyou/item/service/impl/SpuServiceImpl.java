package com.leyou.item.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.leyou.common.dto.PageDTO;
import com.leyou.common.exceptions.LyException;
import com.leyou.item.dto.SkuDTO;
import com.leyou.item.dto.SpuDTO;
import com.leyou.item.dto.SpuDetailDTO;
import com.leyou.item.entity.*;
import com.leyou.item.mapper.SpuMapper;
import com.leyou.item.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SpuServiceImpl extends ServiceImpl<SpuMapper, Spu> implements SpuService {

    @Autowired
    private  final BrandService brandService;
    @Autowired
    private  final CategoryService categoryService;
    @Autowired
    private  final SpuDetailService spuDetailService;
    @Autowired
    private  final SkuService skuService;

    public SpuServiceImpl(BrandService brandService, CategoryService categoryService, SpuDetailService spuDetailService, SkuService skuService) {
        this.brandService = brandService;
        this.categoryService = categoryService;
        this.spuDetailService = spuDetailService;
        this.skuService = skuService;
    }

    @Override
    public PageDTO<SpuDTO> querySpuByPage(Integer page, Integer rows, Boolean saleable, Long categoryId, Long brandId, Long id) {
        // 1.分页信息
        page = Math.max(Math.min(page, 100), 1);
        rows = Math.max(rows, 5);

        // 2.查询条件
        Spu spu = new Spu();
        spu.setId(id);
        spu.setCid3(categoryId);
        spu.setBrandId(brandId);
        spu.setSaleable(saleable);
        QueryWrapper<Spu> queryWrapper = new QueryWrapper<>(spu);
        // 3.查询数据
        IPage<Spu> result = page(new Page<>(page, rows), queryWrapper);

        // 4.处理结果
        List<Spu> records = result.getRecords();
        if (CollectionUtils.isEmpty(records)) {
            throw new LyException(400, "商品id不存在");
        }
        // 4.1.DTO转换
        List<SpuDTO> list = SpuDTO.convertEntityList(records);
        // 4.2.处理分类和品牌名称
        for (SpuDTO spuDTO : list) {
            handleCategoryAndBrandName(spuDTO);
        }

        return new PageDTO<>(result.getTotal(), result.getPages(), list);
    }

    @Override
    public void saveGoods(SpuDTO spuDTO) {
        // 1.新增Spu
        Spu spu = spuDTO.toEntity(Spu.class);
        spu.setSaleable(false);
        boolean success = save(spu);
        if (!success) {
            throw new LyException(500, "新增商品失败");
        }
        // 2.新增SpuDetail
        SpuDetail spuDetail = spuDTO.getSpuDetail().toEntity(SpuDetail.class);
        spuDetail.setSpuId(spu.getId());
        success = spuDetailService.save(spuDetail);
        if (!success) {
            throw new LyException(500, "新增商品详情失败");
        }
        // 3.新增Sku
        List<Sku> list = spuDTO.getSkus().stream().map(skuDTO -> {
            Sku sku = skuDTO.toEntity(Sku.class);
            sku.setSaleable(false);
            sku.setSpuId(spu.getId());
            return sku;
        }).collect(Collectors.toList());
        // 批量新增
        skuService.saveBatch(list);
    }

    @Override
    @Transactional
    public void updateSaleable(Long id, Boolean saleable) {
        // 1.更新SPU
        Spu spu = new Spu();
        spu.setId(id);
        spu.setSaleable(saleable);
        boolean success = updateById(spu);
        if (!success) {
            throw new LyException(500, "更新失败");
        }
        // 2.更新sku
        success =skuService.update().eq("spu_id", id).set("saleable", saleable).update();
        if (!success) {
            throw new LyException(500, "更新失败");
        }
    }

    @Override
    public SpuDTO queryGoodsById(Long id) {
        // 1.查询spu
        // 1.1.查询
        Spu spu = getById(id);
        if (spu == null) {
            throw new LyException(400, "商品id不存在！");
        }
        // 1.2.转换DTO
        SpuDTO spuDTO = new SpuDTO(spu);

        // 2.查询spuDetail
        SpuDetail detail = spuDetailService.getById(id);
        if (detail == null) {
            throw new LyException(400, "商品id不存在！");
        }
        spuDTO.setSpuDetail(new SpuDetailDTO(detail));

        // 3.查询sku
        List<Sku> list = skuService.query().eq("spu_id", id).list();
        if(CollectionUtils.isEmpty(list)){
            throw new LyException(400, "商品id不存在！");
        }
        spuDTO.setSkus(SkuDTO.convertEntityList(list));

        // 4.准备商品分类和品牌名称
        handleCategoryAndBrandName(spuDTO);

        return spuDTO;
    }

    @Override
    public void updateGoods(SpuDTO spuDTO) {
        // 1.更新spu
        // 判断是否包含id，如果包含则更新spu
        if (spuDTO.getId() != null) {
            Spu spu = spuDTO.toEntity(Spu.class);
            boolean success = updateById(spu);
            if (!success) {
                throw new LyException(500, "更新失败");
            }
        }

        // 2.更新detail
        // 2.1.获取detail
        SpuDetailDTO spuDetailDTO = spuDTO.getSpuDetail();
        // 2.2.判断是否包含detail，没有则不更新
        if (spuDetailDTO != null) {
            SpuDetail spuDetail = spuDetailDTO.toEntity(SpuDetail.class);
            boolean success = spuDetailService.updateById(spuDetail);
            if (!success) {
                throw new LyException(500, "更新失败");
            }
        }

        // 3.更新sku
        // 3.1.获取sku
        List<SkuDTO> dtoList = spuDTO.getSkus();
        // 3.2.判断是否存在
        if (CollectionUtils.isEmpty(dtoList)) {
            return;
        }
        // 3.3.分离包含id的sku和不包含id的sku
        Map<Boolean, List<Sku>> skuList = dtoList.stream()
                .map(skuDTO -> skuDTO.toEntity(Sku.class))
                // 根据id是否null分离
                .collect(Collectors.partitioningBy(sku -> sku.getId() == null));
        // 3.4.新增sku
        List<Sku> insertList = skuList.get(true);
        if (!CollectionUtils.isEmpty(insertList)) {
            boolean success = skuService.saveBatch(insertList);
            if (!success) {
                throw new LyException(500, "更新失败");
            }
        }
        // 3.5.更新sku
        List<Sku> updateList = skuList.get(false);
        if (!CollectionUtils.isEmpty(updateList)) {
            boolean success = skuService.updateBatchById(updateList);
            if (!success) {
                throw new LyException(500, "更新失败");
            }
        }
    }

    private void handleCategoryAndBrandName(SpuDTO spuDTO) {
        // 处理品牌名称
        Brand brand = brandService.getById(spuDTO.getBrandId());
        if (brand != null) {
            spuDTO.setBrandName(brand.getName());
        }
        // 处理分类名称
        Collection<Category> categories = categoryService.listByIds(spuDTO.getCategoryIds());
        if (!CollectionUtils.isEmpty(categories)) {
            spuDTO.setCategoryName(
                    categories.stream().map(Category::getName).collect(Collectors.joining("/")));
        }
    }
}
