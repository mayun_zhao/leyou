package com.leyou.item.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.leyou.common.entiry.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@TableName("tb_spec_group")
@Data
@EqualsAndHashCode(callSuper = true)
public class SpecGroup extends BaseEntity {
    @TableId
    private Long id;
    private Long categoryId;
    private String name;
}
