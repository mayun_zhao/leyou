package com.leyou.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leyou.item.dto.SpecParamDTO;
import com.leyou.item.entity.SpuDetail;

import java.util.List;

public interface SpuDetailService  extends IService<SpuDetail> {
    List<SpecParamDTO> querySpecValues(Long id, Boolean searching);
}
