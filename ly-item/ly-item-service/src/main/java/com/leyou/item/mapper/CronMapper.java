package com.leyou.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

public interface CronMapper extends BaseMapper {
    @Select("select cron from cron limit 1")
    String getCron();

    @Select("select cron from cron where cron_id = 2")
    String getCron2();
}
