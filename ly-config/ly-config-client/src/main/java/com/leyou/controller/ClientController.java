package com.leyou.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 通知spring容器热刷新的时候重新刷新当前类型对应的所有对象
 * spring容器为提升热刷新效率,默认不刷新对象内容,只有明确指定的对象,才刷新
 */
@RestController
@RefreshScope
public class ClientController {

    @Value("${my.args.word}")
    private String word;

    @Value("${my.args.test}")
    private String test;

    @RequestMapping("/hello")
    public String index() {
        return this.word+this.test;
    }
}


