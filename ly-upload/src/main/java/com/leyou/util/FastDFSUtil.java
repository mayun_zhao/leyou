package com.leyou.util;

import com.leyou.entiry.FastDFSFile;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

public class FastDFSUtil {
    /**
     * 加载Tracker衔接信息
     */
    static{
        try {
            //查找Classpath下的文件路径
            String fileName=new ClassPathResource("fdfs_client.conf").getPath();
            ClientGlobal.init(fileName);
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 文件上传
     * @param fastDFSFile
     */
    public  static void upload(FastDFSFile fastDFSFile) throws Exception {
        //附加参数
        NameValuePair[] data_list=new NameValuePair[1];
        data_list[0]= new NameValuePair("author",fastDFSFile.getAuther());

        //创建一个Tracker访问的客户端TrackerClient
        TrackerClient trackerClient = new TrackerClient();

        //通过TrackerClient访问TrackerServer服务,获取连接信息
        TrackerServer trackerServer = trackerClient.getConnection();

        //通过TrackerServer的链接信息可以获取Storage的链接信息,创建StroageClient对象存储Stroage的链接信息
        StorageClient storageClient = new StorageClient(trackerServer, null);

        /**
         *通过StroageClient访问Stroage,实现文件上传,并且获取文件上传后的存储信息
         * 1:上传文件的字节数据
         * 2.文件的扩展名
         * 3.附加参数 比如拍摄地址,上海
         */
        storageClient.upload_file(fastDFSFile.getContent(),fastDFSFile.getExt(),data_list);
    }
}
