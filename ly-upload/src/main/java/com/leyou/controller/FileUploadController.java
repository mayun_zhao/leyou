package com.leyou.controller;


import com.leyou.entiry.FastDFSFile;
import com.leyou.util.FastDFSUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import java.text.SimpleDateFormat;
import java.util.Date;


@RestController
@RequestMapping(value = "/upload")
@CrossOrigin
public class FileUploadController {

    /**
     * 文件上传
     */
    @PostMapping
    public ResponseEntity<?> upload(@RequestParam(value = "files") MultipartFile files) {
        //封装文件信息
        FastDFSFile fastDFSFile = null;
        try {
            fastDFSFile = new FastDFSFile(
                    files.getName()  //文件名称
                    , files.getBytes() //文件字节数组
                    , StringUtils.getFilenameExtension(files.getName())//文件扩展名
            );

        //调用FastDFSUtil工具类将文件传入到fastDFS中

        FastDFSUtil.upload(fastDFSFile);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return new ResponseEntity("上传成功", HttpStatus.OK);
    }
    @GetMapping
    public  ResponseEntity<?> testJemeter() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Thread.currentThread().sleep(1000);
        String format = dateFormat.format(new Date());
        System.out.println(format);
        return new ResponseEntity("测试jetemter"+format,HttpStatus.OK);
    }
}
