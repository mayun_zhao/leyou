package com.leyou.entiry;

import java.io.Serializable;

/**
 * 文件上传封装信息
 */
public class FastDFSFile implements Serializable {
    /**
     * 文件名称
     */
    private  String name;
    /**
     * 文件内容
     */
    private byte[] content;
    /**
     * 文件扩展名
     */
    private String ext;
    /**
     * 文件MD5摘要值
     */
    private String md5;
    /**
     * 文件创作者
     */
    private String auther;

    public FastDFSFile(String name, byte[] content, String ext, String md5, String auther) {
        this.name = name;
        this.content = content;
        this.ext = ext;
        this.md5 = md5;
        this.auther = auther;
    }

    public FastDFSFile(String name, byte[] bytes, String filenameExtension) {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getAuther() {
        return auther;
    }

    public void setAuther(String auther) {
        this.auther = auther;
    }
}
