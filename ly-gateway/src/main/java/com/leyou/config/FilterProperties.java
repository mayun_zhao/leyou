package com.leyou.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

/**
 * @author 虎哥
 */
@Data
@Component
@ConfigurationProperties("ly.filter")
public class FilterProperties {
    private Map<String, Set<String>> allowRequests;
}
