package com.leyou.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.leyou.common.exceptions.LyException;
import com.leyou.common.utils.RegexUtils;
import com.leyou.user.dto.UserDTO;
import com.leyou.user.entiry.User;
import com.leyou.user.mapper.UserMapper;
import com.leyou.user.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.leyou.common.constants.MQConstants.ExchangeConstants.SMS_EXCHANGE_NAME;
import static com.leyou.common.constants.MQConstants.RoutingKeyConstants.VERIFY_CODE_KEY;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    private StringRedisTemplate redisTemplate;

    private AmqpTemplate amqpTemplate;

    private final BCryptPasswordEncoder passwordEncoder;

    private static final String KEY_PREFIX_CODE = "user:code:phone:";
    public UserServiceImpl(StringRedisTemplate redisTemplate, AmqpTemplate amqpTemplate, BCryptPasswordEncoder passwordEncoder) {
        this.redisTemplate = redisTemplate;
        this.amqpTemplate = amqpTemplate;
        this.passwordEncoder = passwordEncoder;
    }
    @Override
    public Boolean exists(String data, Integer type) {
        if(type != 1&& type != 2){
            throw new LyException(400,"请求参数有误");
        }
        return  query()
                .eq(type ==1,"username",data)
                .eq(type == 2,"phone" ,data)
                .count() == 1;
    }

    @Override
    public void sendCode(String phone) {
        // 1.验证手机号格式
        if (!RegexUtils.isPhone(phone)) {
            throw new LyException(400, "请求参数有误");
        }

        // 2.使用Apache的工具类生成6位数字验证码
        String code = RandomStringUtils.randomNumeric(6);

        // 3.保存验证码到redis
        redisTemplate.opsForValue().set(KEY_PREFIX_CODE + phone, code, 5, TimeUnit.MINUTES);

        // 4.发送RabbitMQ消息到ly-sms
        Map<String, String> msg = new HashMap<>();
        msg.put("phone", phone);
        msg.put("code", code);
        amqpTemplate.convertAndSend(SMS_EXCHANGE_NAME, VERIFY_CODE_KEY, msg);
    }

    @Override
    @Transactional
    public void register(User user, String code) {
        // 1.校验验证码
        // 1.1 取出redis中的验证码
        String cacheCode = redisTemplate.opsForValue().get(KEY_PREFIX_CODE + user.getPhone());
        // 1.2 比较验证码
        if (!StringUtils.equals(code, cacheCode)) {
            throw new LyException(400, "验证码错误");
        }
        // 2.对密码加密
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // 3.写入数据库
        save(user);
    }
    public void register() {
        User user = new User();
        user.setUsername("lisi");
        user.setPassword("123456");
        user.setPhone("15928965476");
        // 2.对密码加密
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        // 3.写入数据库
        save(user);
    }


    @Override
    public UserDTO loginCheck(User user) {
        User result = query().eq("username", user.getUsername()).one();
        if(user==null){
            throw new LyException(400,"用户名不正确");
        }
        if(!passwordEncoder.matches(user.getPassword(),result.getPassword())){
            throw new LyException(400,"密码不正确");
        }
        return new UserDTO(result);
    }
}
