package com.leyou.auth.constant;


public class UserTokenConstants {
    /**
     * 用户token的cookie名称
     */
    public static final String COOKIE_NAME = "LY_TOKEN";
    /**
     * 用户token的cookie的domain
     */
    public static final String DOMAIN = "leyou.com";
}
