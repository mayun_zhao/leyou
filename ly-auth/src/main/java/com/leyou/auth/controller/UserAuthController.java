package com.leyou.auth.controller;

import com.leyou.auth.service.UserAuthService;
import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/auth")
public class UserAuthController {

    @Autowired
    private UserAuthService userAuthService;

    /**
     * 登录接口
     * @param username
     * @param password
     * @param response
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<Void> login(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            HttpServletResponse response){
        // 调用service，完成登录
        userAuthService.login(username, password, response);
        // 登录成功，无返回值, 204状态码
        return ResponseEntity.noContent().build();
    }
    @GetMapping("/verify")
    public ResponseEntity<String> verifyUser(HttpServletRequest request){
        return ResponseEntity.ok(userAuthService.verifyUser(request));
    }


    @PostMapping("/loginOut")
    public ResponseEntity<Void> loginOut(HttpServletRequest request, HttpServletResponse response){
        userAuthService.loginOut(request, response);
        return ResponseEntity.noContent().build();
    }
}
