package com.leyou.auth.service.impl;

import com.leyou.auth.config.JwtProperties;
import com.leyou.auth.constant.UserTokenConstants;
import com.leyou.auth.service.UserAuthService;
import com.leyou.common.constants.RedisConstants;
import com.leyou.common.entiry.Payload;
import com.leyou.common.entiry.UserInfo;
import com.leyou.common.exceptions.LyException;
import com.leyou.common.utils.CookieUtils;
import com.leyou.common.utils.JwtUtils;
import com.leyou.user.client.UserClient;
import com.leyou.user.dto.UserDTO;
import feign.FeignException;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

@Service
public class UserAuthServiceImpl implements UserAuthService {

    private  final UserClient userClient;

    private final JwtProperties prop;

    private final StringRedisTemplate redisTemplate;

    public UserAuthServiceImpl(UserClient userClient, JwtProperties prop, StringRedisTemplate redisTemplate) {
        this.userClient = userClient;
        this.prop = prop;
        this.redisTemplate = redisTemplate;
    }


    @Override
    public void login(String username, String password, HttpServletResponse response) {
        UserDTO user=null;
        try {
            user=userClient.queryUserByUsernameAndPassword(username,password);
        }catch (FeignException e){
            throw new LyException(e.status(),e.contentUTF8(),e);
        }
        if(null==user){
            throw new LyException(400,"用户名账户和密码错误!");
        }
        String jti = JwtUtils.createJTI();
        String token = JwtUtils.generateToken(new UserInfo(user.getId(), user.getUsername()), prop.getPrivateKey());
        redisTemplate.opsForValue().set(
                RedisConstants.JTI_KEY_PREFIX+user.getId(),jti,RedisConstants.TOKEN_EXPIRE_MINUTES, TimeUnit.MINUTES);
        CookieUtils.builder(response)
                .httpOnly(true)
                .domain(UserTokenConstants.DOMAIN)
                .build(com.leyou.auth.constant.UserTokenConstants.COOKIE_NAME,token);
    }

    @Override
    public String verifyUser(HttpServletRequest request) {
        // 1.服务端接收请求，获取cookie
        String token = CookieUtils.getCookieValue(request, UserTokenConstants.COOKIE_NAME);
        if (StringUtils.isBlank(token)) {
            throw new LyException(400, "未登录或者登录已经过期");
        }
        // 2.验证cookie中的token，并获取其中的信息
        Payload<UserInfo> payload = null;
        try {
            payload = JwtUtils.getInfoFromToken(token, prop.getPublicKey(), UserInfo.class);
        } catch (ExpiredJwtException e) {
            // 无效的token，返回400
            throw new LyException(400, "未登录或者登录已经过期");
        }
        UserInfo userInfo = payload.getUserInfo();
        String key = RedisConstants.JTI_KEY_PREFIX + userInfo.getId();
        String cacheJTI = redisTemplate.opsForValue().get(key);
        if (!StringUtils.equals(cacheJTI, payload.getId())) {
            // token的id不符合，证明token无效，直接返回401
            throw new LyException(401, "登录失效");
        }
        // 3.返回用户名
        return payload.getUserInfo().getUsername();
    }

    @Override
    public void loginOut(HttpServletRequest request, HttpServletResponse response) {
        String cookieValue = CookieUtils.getCookieValue(request, UserTokenConstants.COOKIE_NAME);
        Payload<UserInfo> payload=null;
        try {
            Payload<UserInfo> infoFromToken = JwtUtils.getInfoFromToken(cookieValue, prop.getPublicKey(), UserInfo.class);
        }catch (Exception e){
            return;
        }
        CookieUtils.deleteCookie(com.leyou.common.constants.UserTokenConstants.COOKIE_NAME,UserTokenConstants.DOMAIN,response);
        UserInfo userInfo = payload.getUserInfo();
        redisTemplate.delete(RedisConstants.JTI_KEY_PREFIX+userInfo.getId());
    }
}
